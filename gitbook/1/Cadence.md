# Cadence
## 1、常用EDA软件
 
  * [Cadence Allegro](https://www.cadence.com/en_US/home/tools/pcb-design-and-analysis/pcb-layout/allegro-pcb-designer.html)  主流、详细、上手难、适合多层高速板.合并了OrCAD
  
  * [Altium_Designer](https://www.altium.com.cn)  主流、视图化、上手简单、适合6层左右
 
 * 其他软件：protel、pads、KiCad

 * 阻抗设计软件：Shortcut to Si9000
 ![](https://gitlab.com/picturenick2/forpicgo/uploads/8e6d6f3e87c962ea93acfb25f8f512a0/QQ截图20200407130540.png)

  ## 2、Cadence安装 
  * 安装软件  [阿狸狗](http://tools.mr-wu.cn/#thirdPage)
  * 安装步骤：
    
    1、选择AleegoCrackMasterV3
    ![](https://gitlab.com/picturenick2/forpicgo/uploads/5d8a567c904e0ac6f830c408f5d06ebe/aleego.png)

    2、安装cadence主程序、自动安装
    ![](https://gitlab.com/picturenick2/forpicgo/uploads/40b42890fc2ab256e92df6f576654f95/微信截图_20200407105915.png)

    3、安装补丁
   ![](https://gitlab.com/picturenick2/forpicgo/uploads/3c848d24c4d1eeadf17e983d2e93c81e/QQ截图20200407110038.png)

    4、安装成功后、可在开始处看到Cadence各模块
   ![](https://gitlab.com/picturenick2/forpicgo/uploads/24936056e8ed1369f96753427b3e53ee/QQ截图20200407111131.png)

   

## Cadence 使用

* 板级原理图选择 
  ![](https://gitlab.com/picturenick2/forpicgo/uploads/6977ae4c4f3f7c36c54aba122a255af8/原理图.png)

* 元件焊盘制作选择
  ![](https://gitlab.com/picturenick2/forpicgo/uploads/6b7020708feb609f27f3518d49534a8b/焊盘.png)

* 元件封装制作
   
   1、选择PCB Editor模块![](https://gitlab.com/picturenick2/forpicgo/uploads/93fa7742d44f0a461f340e3f03c4a57c/封装.png)
   2、选择ackage symbol
   ![](https://gitlab.com/picturenick2/forpicgo/uploads/30539f45f00c0beeb38b3b8e6ead8c16/QQ截图20200407125808.png)

* PCB layout设计
  
  1、选择PCB Editor模块![](https://gitlab.com/picturenick2/forpicgo/uploads/93fa7742d44f0a461f340e3f03c4a57c/封装.png)
  2、选择Board
 ![](https://gitlab.com/picturenick2/forpicgo/uploads/1a0b1ebec5cdeae403613aa4f268074a/QQ截图20200407130250.png)



